﻿using UnityEngine;
using System.Collections;

public class WinLose : MonoBehaviour {

	public AudioClip scream;
	public GameObject fadingPanel;

	private LevelManager levelManager;
	private PlayerController player;
	private AudioSource audioSource;
	private WaitForSeconds deathAnim = new WaitForSeconds(3.3f);
	private WaitForSeconds winAnim = new WaitForSeconds(3.1f);

	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		audioSource = GetComponent<AudioSource>();
		player = GetComponent<PlayerController>();
	}

	public void PlayerDead() {
		player.canMove = false;
		fadingPanel.GetComponent<Animator>().SetBool("Dead", true);
		audioSource.clip = scream;
		audioSource.Play();
		StartCoroutine(GameOver());
	}

	IEnumerator GameOver() {
		yield return deathAnim;
		levelManager.LoadNextLevel();
	}

	public void PlayerEscapes() {
		player.canMove = false;
		GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
		foreach (GameObject zombie in zombies) {
			zombie.GetComponent<NavMeshAgent>().speed = 0f;
		}
		fadingPanel.GetComponent<Animator>().SetBool("Win", true);
		StartCoroutine(Win());
	}

	IEnumerator Win() {
		yield return winAnim;
		levelManager.LoadLevel("Win");
	}
}
