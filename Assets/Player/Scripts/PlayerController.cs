using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


[RequireComponent(typeof (CharacterController))]
[RequireComponent(typeof (AudioSource))]
public class PlayerController : MonoBehaviour
{

	public bool canMove;

	[SerializeField] private float walkSpeed;
	[SerializeField] private float runSpeed;
    [SerializeField] private bool isWalking;
	[SerializeField] private float standingHeight;
    [SerializeField] private float crouchingHeight; 
    [SerializeField] private float standingSpeed;
    [SerializeField] [Range(0f, 1f)] private float runstepLenghten;
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float stickToGroundForce;
    [SerializeField] private float gravityMultiplier;
    [SerializeField] private MouseLook mouseLook;
    [SerializeField] private bool useFovKick;
    [SerializeField] private FOVKick fovKick = new FOVKick();
    [SerializeField] private bool useHeadBob;
    [SerializeField] private CurveControlledBob headBob = new CurveControlledBob();
    [SerializeField] private LerpControlledBob jumpBob = new LerpControlledBob();
    [SerializeField] private float stepInterval;
    [SerializeField] private AudioClip[] footstepSounds;    
    [SerializeField] private AudioClip jumpSound;           
    [SerializeField] private AudioClip landSound;
         

    private Camera playerCamera;
    private bool jump;
    private float yRotation;
    private Vector2 input;
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController characterController;
    private CollisionFlags collisionFlags;
    private bool previouslyGrounded;
    private Vector3 originalCameraPosition;
    private float stepCycle;
    private float nextStep;
    private bool jumping;
    private AudioSource audioSource;
    private bool crouching;
    private bool standing;
    private bool crouched;
    private float timeParam = 0;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        playerCamera = Camera.main;
        originalCameraPosition = playerCamera.transform.localPosition;
        fovKick.Setup(playerCamera);
        headBob.Setup(playerCamera, stepInterval);
        stepCycle = 0f;
        nextStep = stepCycle/2f;
        jumping = false;
        crouching = false;
        crouched = false;
        standing = true;
        audioSource = GetComponent<AudioSource>();
		mouseLook.Init(transform , playerCamera.transform);
		canMove = false;
    }


    void Update()
    {
    	if(canMove) {
	        RotateView();

			if(!crouching && !crouched)
	        {
				crouching = CrossPlatformInputManager.GetButtonDown("Crouch");
	        }

	        if(!standing) {
	        	standing = CrossPlatformInputManager.GetButtonDown("Crouch");
	        }


	        if (!jump)
	        {
	            jump = CrossPlatformInputManager.GetButtonDown("Jump");
	        }

	        if (!previouslyGrounded && characterController.isGrounded)
	        {
	            StartCoroutine(jumpBob.DoBobCycle());
	            PlayLandingSound();
	            moveDirection.y = 0f;
	            jumping = false;
	        }
	        if (!characterController.isGrounded && !jumping && previouslyGrounded)
	        {
	            moveDirection.y = 0f;
	        }

	        previouslyGrounded = characterController.isGrounded;
	    }
    }


    void PlayLandingSound()
    {
        audioSource.clip = landSound;
        audioSource.Play();
        nextStep = stepCycle + .5f;
    }


    void FixedUpdate()
    {
    	if(canMove) {
	        float speed;
	        GetInput(out speed);
	        // always move along the camera forward as it is the direction that it being aimed at
	        Vector3 desiredMove = transform.forward*input.y + transform.right*input.x;

	        // get a normal for the surface that is being touched to move along it
	        RaycastHit hitInfo;
	        Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
	                           characterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
	        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

	        moveDirection.x = desiredMove.x*speed;
	        moveDirection.z = desiredMove.z*speed;


	        if (characterController.isGrounded)
	        {
	            moveDirection.y = -stickToGroundForce;

	            if (jump)
	            {
	                moveDirection.y = jumpSpeed;
	                PlayJumpSound();
	                jump = false;
	                jumping = true;
	            }


	            if(crouching)
	            {  
	            	timeParam += standingSpeed * Time.deltaTime;
					characterController.height = Mathf.Lerp(standingHeight, crouchingHeight, timeParam);
					if(crouchingHeight == characterController.height)
					{
						crouched = true;
						crouching = false;
						standing = false;
						timeParam = 0;
					}
	            }

	            if(crouched && standing)
	            {
					
					timeParam += (standingSpeed * 20) * Time.deltaTime;
					characterController.height = Mathf.Lerp(crouchingHeight, standingHeight, timeParam);
					if(standingHeight == characterController.height)
					{
						crouched = false;
						timeParam = 0;
					}
	            }


	        }
	        else
	        {
	            moveDirection += Physics.gravity*gravityMultiplier*Time.fixedDeltaTime;
	        }
	        collisionFlags = characterController.Move(moveDirection*Time.fixedDeltaTime);

	        ProgressStepCycle(speed);
	        UpdateCameraPosition(speed);
	    }

		mouseLook.UpdateCursorLock();
    }


    void PlayJumpSound()
    {
        audioSource.clip = jumpSound;
        audioSource.Play();
    }


    void ProgressStepCycle(float speed)
    {
        if (characterController.velocity.sqrMagnitude > 0 && (input.x != 0 || input.y != 0))
        {
            stepCycle += (characterController.velocity.magnitude + (speed*(isWalking ? 1f : runstepLenghten)))*
                         Time.fixedDeltaTime;
        }

        if (!(stepCycle > nextStep))
        {
            return;
        }

        nextStep = stepCycle + stepInterval;

        PlayFootStepAudio();
    }


    void PlayFootStepAudio()
    {
        if (!characterController.isGrounded)
        {
            return;
        }
  
        int n = Random.Range(1, footstepSounds.Length);
        audioSource.clip = footstepSounds[n];
        audioSource.PlayOneShot(audioSource.clip);

        footstepSounds[n] = footstepSounds[0];
        footstepSounds[0] = audioSource.clip;
    }


    void UpdateCameraPosition(float speed)
    {
        Vector3 newCameraPosition;
        if (!useHeadBob)
        {
            return;
        }
        if (characterController.velocity.magnitude > 0 && characterController.isGrounded)
        {
            playerCamera.transform.localPosition =
                headBob.DoHeadBob(characterController.velocity.magnitude +
                                  (speed*(isWalking ? 1f : runstepLenghten)));
            newCameraPosition = playerCamera.transform.localPosition;
            newCameraPosition.y = playerCamera.transform.localPosition.y - jumpBob.Offset();
        }
        else
        {
            newCameraPosition = playerCamera.transform.localPosition;
            newCameraPosition.y = originalCameraPosition.y - jumpBob.Offset();
        }
        playerCamera.transform.localPosition = newCameraPosition;
    }


    void GetInput(out float speed)
    {
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        bool waswalking = isWalking;

#if !MOBILE_INPUT
        
        isWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
        
        speed = isWalking ? walkSpeed : runSpeed;
        input = new Vector2(horizontal, vertical);

        if (input.sqrMagnitude > 1)
        {
            input.Normalize();
        }

        if (isWalking != waswalking && useFovKick && characterController.velocity.sqrMagnitude > 0)
        {
            StopAllCoroutines();
            StartCoroutine(!isWalking ? fovKick.FOVKickUp() : fovKick.FOVKickDown());
        }
    }


    void RotateView()
    {
        mouseLook.LookRotation (transform, playerCamera.transform);
    }


    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        if (collisionFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(characterController.velocity*0.1f, hit.point, ForceMode.Impulse);
    }
}

