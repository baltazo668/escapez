﻿using UnityEngine;
using System.Collections;

public class UseFlare : MonoBehaviour {

	public bool flareFound;
	public GameObject flareParticles;
	public Transform flareDropPosition;

	[SerializeField] private float positionOffset = 2f;

	private UIDisplayManager uiDisplay;
	private Helicopter chopper;
	private float nextTryTime;
	private float placeRate = 1.5f;

	void Start () {
		uiDisplay = GameObject.FindObjectOfType<UIDisplayManager>();
		chopper = GameObject.FindObjectOfType<Helicopter>();
		flareFound = false;
	}

	void Update () {
		if(flareFound && Input.GetButtonDown("Flare") && Time.time > nextTryTime) {
			Vector3 center = new Vector3(transform.position.x, (transform.position.y + positionOffset), transform.position.z);
			Vector3 extent = new Vector3(5, 1, 15);
			if(Physics.CheckBox(center, extent) == false) {
				/*GameObject flare = */Instantiate(flareParticles, flareDropPosition.transform.position, Quaternion.identity)/* as GameObject*/;
				flareFound = false;
				uiDisplay.FlarePlaced();
				chopper.FlarePlaced();
			} else {
				uiDisplay.AreaNotClear();
				nextTryTime = Time.time + placeRate;
			}
		}
	}

}
