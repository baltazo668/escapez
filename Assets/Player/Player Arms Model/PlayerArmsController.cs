﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerArmsController : MonoBehaviour {

	public bool movingLeftArm = false;
	public GameObject flashlight;
	public GameObject leftArmEmpty;
	public GameObject rightArmEmpty;
	public Transform camRotation;
	public bool gunFound;

	private Animator leftArmAnimator;
	private Animator rightArmAnimator;
	private GunShooting gunShoot;
	private bool lightFound;

	void Start () {
		leftArmAnimator = leftArmEmpty.GetComponent<Animator>();
		rightArmAnimator = rightArmEmpty.GetComponent<Animator>();
		gunShoot = GameObject.FindObjectOfType<GunShooting>();
		lightFound = false;
		gunFound = false;
	}

	void Update() {

		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D)) {
			leftArmAnimator.SetBool("Walking", true);
			rightArmAnimator.SetBool("Walking", true);
		}

		if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) {
			leftArmAnimator.SetBool("Walking", false);
			rightArmAnimator.SetBool("Walking", false);
		}

	}

	public void MovingLeftArm() {
		leftArmAnimator.SetTrigger("LightFound");
		flashlight.GetComponent<Light>().enabled = true;
		lightFound = true;
	}

	public void MovingRightArm() {
		rightArmAnimator.SetTrigger("gunPickedUp");;
		Debug.Log("Moving Right arm");
		gunShoot.canShoot = true;
		gunFound = true;
	}

	public void Recharging() {
		rightArmAnimator.SetTrigger("recharging");
		StartCoroutine(ResumeShooting(2f));
	}

	public void AnimationOver() {
		if(lightFound) {
			leftArmAnimator.SetTrigger("AnimationOver");
		}
		if(gunFound) {
			rightArmAnimator.SetTrigger("AnimationOver");
		}
	}

	private IEnumerator ResumeShooting(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		gunShoot.canShoot = true;
		}
	}


