﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof (NavMeshAgent))]
[RequireComponent(typeof (ZombieCharacter))]
public class ZombieAIController : MonoBehaviour {

	public NavMeshAgent agent { get; private set; }     // the navmesh agent for the path finding
    public ZombieCharacter zombie { get; private set; } // the character we are controlling

    private Transform target;  

	void Start () 
	{
            agent = GetComponentInChildren<NavMeshAgent>();
            zombie = GetComponent<ZombieCharacter>();
            target = GameObject.FindGameObjectWithTag("Player").transform;

	        agent.updateRotation = false;
	        agent.updatePosition = true;
	}

	void Update () 
	{
		if (target != null)
		{
                agent.SetDestination(target.position);
        }

        if (agent.remainingDistance > agent.stoppingDistance) 
        {
                zombie.Move(agent.desiredVelocity);
        }
        else
        {
                zombie.Move(Vector3.zero);
        }

        if(zombie.zombieDead) {
        	SetTarget(null);
        }
	}

	public void SetTarget(Transform target)
        {
            this.target = target;
        }
}
