﻿using UnityEngine;
using System.Collections;

public class ZombieDamage : MonoBehaviour {

	public int startHealth = 1; //The starting health for the zombie, tight now 1 if I make different types it could be more
	public GameObject hitParticles;

	private int currentHealth;

	void Start () {
		currentHealth = startHealth;
	}
	
	public void Damage(int damage, Vector3 hitPoint) {
		currentHealth -= damage;
		if(currentHealth <= 0) {
			gameObject.GetComponent<ZombieCharacter>().zombieDead = true;
		}
	}
}
