﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class ZombieCharacter : MonoBehaviour {

	[SerializeField] float movingTurnSpeed = 360;
	[SerializeField] float stationaryTurnSpeed = 180;
	[SerializeField] float sinkingSpeed = 2.5f;

	public AudioClip[] moans;
	public bool zombieDead = false;

	private WinLose playerDeath;
	private Rigidbody rb;
	private Animator anim;
	private ZombieSpawner spawner;
	private bool isGrounded;
	private const float half = 0.5f;
	private float turnAmount;
	private float forwardAmount;
	private Vector3 groundNormal;
	private AudioSource audioSource;
	private float timeSinceLastMoan = 0;
	private bool isSinking = false;
	private WaitForSeconds sinkingTime = new WaitForSeconds(2f);


	void Start () 
	{
		spawner = GameObject.FindObjectOfType<ZombieSpawner>();
		anim = GetComponent<Animator>();
		rb = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource>();
		rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
		playerDeath = GameObject.FindObjectOfType<WinLose>();
	}

	void Update() 
	{
		if(Time.time >= (timeSinceLastMoan + Random.Range(1f, 6f)) && !zombieDead) 
		{
			Moan();
		}

		if(zombieDead) {
			Death();
		}

		if(isSinking) {
			transform.Translate(Vector3.down * sinkingSpeed * Time.deltaTime);
		}
	}

	void Moan() 
	{
		audioSource.clip = moans[Random.Range(0, moans.Length)];
		audioSource.Play();
		timeSinceLastMoan = Time.time;

	}

	public void Move(Vector3 move) 
	{

		if (move.magnitude > 1f) move.Normalize();
		move = transform.InverseTransformDirection(move);
		move = Vector3.ProjectOnPlane(move, groundNormal);
		turnAmount = Mathf.Atan2(move.x, move.z);
		forwardAmount = move.z;

		ApplyExtraTurnRotation();
	}

	void ApplyExtraTurnRotation() 
	{
			// helps the character turn faster
			float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
			transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
		}

	void Death() 
	{
		GetComponent<NavMeshAgent>().enabled = false;
		GetComponent<ZombieAIController>().enabled = false;
		GetComponent<SphereCollider>().enabled = false;
		anim.SetBool("zombieIsDead", true);
		spawner.numberOfZombies--;
		StartCoroutine("Sinking");
	}

	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player") {
			playerDeath.PlayerDead();
		}
	}

	private IEnumerator Sinking() {
		yield return sinkingTime;
		isSinking = true;
		Destroy(gameObject, 2f);
	}
}
