﻿using UnityEngine;
using System.Collections;

public class Helicopter : MonoBehaviour {

	public Transform waitPosition;

	[SerializeField] private float speed = 10f; //TODO Adjust correct speed

	private WinLose escape;
	private UIDisplayManager uiDisplay;
	private Animator chopperAnim;
	private Vector3 chopperWaitVector;
	private GameObject landingArea;
	private Vector3 hoverZone;
	private bool flarePlaced;

	void Start () {
		escape = GameObject.FindObjectOfType<WinLose>();
		uiDisplay = GameObject.FindObjectOfType<UIDisplayManager>();
		chopperAnim = GetComponentInChildren<Animator>();
		chopperWaitVector = new Vector3(waitPosition.transform.position.x, waitPosition.transform.position.y, waitPosition.transform.position.z);
		flarePlaced = false;
	}
	
	public void LiftOff() {
		chopperAnim.SetBool("chopper_called", true);
		Invoke("WaitForFlare", 4f);
	}

	void WaitForFlare() {
		transform.position = chopperWaitVector;
	}

	public void FlarePlaced(){
		landingArea = GameObject.FindWithTag("LandingArea");
		hoverZone = new Vector3(landingArea.transform.position.x, (landingArea.transform.position.y + 30f), landingArea.transform.position.z);
		flarePlaced = true;
	}

	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player"){
			Debug.Log("Player Picked Up");
			escape.PlayerEscapes();
		}
		if(other.gameObject.tag == "LandingArea") {
			uiDisplay.ChopperArrived();
		}
	}

	void Update() {
		if(flarePlaced) {
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, hoverZone, step);
			transform.LookAt(hoverZone); 
		}
	}
}
