﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour {

	public Camera mainCam;
	public Camera boatCam;
	public GameObject spawner;
	public GameObject helicopter;

	private GameObject playerMovement;
	private PlayerArmsController arms;

	void Start () {
		playerMovement = GameObject.FindGameObjectWithTag("Player");
		arms = GameObject.FindObjectOfType<PlayerArmsController>();
		Invoke("StartGame", 2f);
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
	
	void StartGame() {
		playerMovement.GetComponent<PlayerController>().canMove = true;
	}

	public void ChopperCalled(){
		playerMovement.GetComponent<PlayerController>().canMove = false;
		spawner.GetComponent<ZombieSpawner>().enabled = false;
		GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
		foreach (GameObject zombie in zombies) {
			zombie.GetComponent<NavMeshAgent>().speed = 0f;
			zombie.GetComponent<AudioSource>().volume = 0f;
		}
		boatCam.gameObject.SetActive(true);
		mainCam.gameObject.SetActive(false);
		StartCoroutine(ChopperLiftOff());
	}

	IEnumerator ChopperLiftOff() {
		yield return new WaitForSeconds(2f);
		helicopter.GetComponent<Helicopter>().LiftOff();
	}

	public void ResumePlay(){
		mainCam.gameObject.SetActive(true);
		boatCam.gameObject.SetActive(false);
		playerMovement.GetComponent<PlayerController>().canMove = true;
		arms.AnimationOver();
		spawner.GetComponent<ZombieSpawner>().enabled = true;
		GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
		foreach (GameObject zombie in zombies) {
			if(zombie.name == "z_female") {
				zombie.GetComponent<NavMeshAgent>().speed = 2f;
			} else {
				zombie.GetComponent<NavMeshAgent>().speed = 3f;
			}
			zombie.GetComponent<AudioSource>().volume = 0f;
		}
	}
}
