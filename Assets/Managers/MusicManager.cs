﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	public AudioClip[] levelMusicChangeArray;

	private AudioSource music;

		
	void Awake() {
		DontDestroyOnLoad(gameObject);
	}

	void Start() {
		music = GetComponent<AudioSource>();
		if(!PlayerPrefs.HasKey("master_volume")) {
			PlayerPrefsManager.SetMasterVolume(0.5f);
		}
		music.volume = PlayerPrefsManager.GetMasterVolume();
	}
	
	void OnLevelWasLoaded(int level) {

		if(levelMusicChangeArray[level]) {
			music.clip = levelMusicChangeArray[level];
			music.loop = true;
			music.Play();
		}

	}

	public void ChangeVolume(float volume) {
		music.volume = volume;
	}
}
