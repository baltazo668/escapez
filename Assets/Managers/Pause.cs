﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

	public Text crosshair;
	public Text pauseTitle;
	public Text infoTxt;
	public Button resumeBtn;
	public Button quitBtn;
	public GameObject player;

	private bool isPaused;
	private LevelManager levelManager;
	private GunShooting gun;

	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		gun = GameObject.FindObjectOfType<GunShooting>();
		isPaused = false;
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			isPaused = !isPaused;

			if(isPaused) {
				Time.timeScale = 0;
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
				gun.isPaused = true;
				crosshair.gameObject.SetActive(false);
				infoTxt.gameObject.SetActive(false);
				pauseTitle.gameObject.SetActive(true);
				resumeBtn.gameObject.SetActive(true);
				quitBtn.gameObject.SetActive(true);
				player.GetComponent<PlayerController>().canMove = false;
			}

			if(!isPaused) {
				ResumePlay();
			}
		}
	}

	public void ResumePlay() {
		Time.timeScale = 1;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		crosshair.gameObject.SetActive(true);
		infoTxt.gameObject.SetActive(true);
		pauseTitle.gameObject.SetActive(false);
		resumeBtn.gameObject.SetActive(false);
		quitBtn.gameObject.SetActive(false);
		player.GetComponent<PlayerController>().canMove = true;
		gun.isPaused = false;
	}

	public void Quit() {
		Time.timeScale = 1;
		levelManager.LoadLevel("Main_Menu");
	}
}


