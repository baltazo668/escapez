﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void LoadGame() {
		StartCoroutine(Loading());
	}

	public void LoadLevel(string levelName) {
		SceneManager.LoadScene(levelName);
	}

	public void QuitRequest() {
		Debug.Log("Quit requested");
		StartCoroutine(ExitGame());
	}

	IEnumerator ExitGame() {
		yield return new WaitForEndOfFrame();
		Debug.Log("Quit requested");
		Application.Quit();
	}

	public void LoadNextLevel() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
	}

	IEnumerator Loading() {
		yield return new WaitForSeconds(2f);
		SceneManager.LoadSceneAsync("Level_01");
	}

}
