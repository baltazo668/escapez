﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIDisplayManager : MonoBehaviour {

	public Text interactionText;
	public Text ammoText;
	public Text infoText;
	public GameObject fadingPanel;
	public bool hasGun = false;
	public GameObject animManager;

	private bool interactionFadingIn = false;
	private bool interactionFadingOut = false;
	private bool infoFadingIn = false;
	private bool infoFadingOut = false;
	private bool ammoFadingIn = false;
	private bool gameStarted = false;
	[SerializeField] private float fadeSpeed = 1f;
	private Vector4 textColor = new Vector4(0.9f, 0.9f, 0.9f, 1f);
	private WaitForSeconds firstDisplayWaitTime = new WaitForSeconds(2f);
	private WaitForSeconds displayWaitTime = new WaitForSeconds(3f);
	private WaitForSeconds fastDisplayTime = new WaitForSeconds(0.8f);

	IEnumerator StartGame() {
		gameStarted = true;
		yield return firstDisplayWaitTime;
		infoText.text = "What happened? Where am I?";
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
		yield return displayWaitTime;
		infoText.text = "I was on the boat when...";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
		yield return displayWaitTime;
		infoText.text = "I need to get some help.";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
	}

	public void Interaction(string objectName) {
		interactionText.text = "Press E to pick up the " + objectName;
		interactionFadingIn = true;
		interactionFadingOut = false;
	}

	public void DisplayAmmo() {
		ammoText.text = "6 | 0";
		ammoFadingIn = true;
		hasGun = true;
		StartCoroutine(GunInstructions());
	}

	public void FlareFound() {
		StartCoroutine(RadioThoughts());
	}

	public void AreaNotClear() {
		StartCoroutine(AreaInfo());
	}

	public void FlarePlaced() {
		StartCoroutine(RescueEnRoute());
	}

	public void ChopperArrived() {
		StartCoroutine(HowToEscape());
	}

	IEnumerator HowToEscape() {
		infoText.text = "Step into the smoke to escape!";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
	}

	IEnumerator RescueEnRoute() {
		infoText.text = "The helicopter will be able to find me now.";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
		yield return fastDisplayTime;
		infoText.text = "I just need to survive until then...";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
	}

	IEnumerator AreaInfo() {
		infoText.text = "I can't place the flare here";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return fastDisplayTime;
		infoFadingIn = false;
		infoFadingOut = true;
	}

	IEnumerator RadioThoughts() {
		infoText.text = "I can call for help with this!";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return fastDisplayTime;
		animManager.GetComponent<AnimationManager>().ChopperCalled();
		yield return fastDisplayTime;
		infoFadingIn = false;
		infoFadingOut = true;
		yield return fastDisplayTime;
		infoText.text = "Help is on its way...";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
		yield return firstDisplayWaitTime;
		animManager.GetComponent<AnimationManager>().ResumePlay();
		StartCoroutine(FlareInstructions());
		infoText.text = "I need to place this flare on open ground!";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
		yield return fastDisplayTime;
		infoText.text = "Then the rescue team will be able to find me.";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
	}

	IEnumerator FlareInstructions() {
		yield return firstDisplayWaitTime;
		interactionText.text = "Press F to place the flare in a suitable area";
		interactionFadingIn = true;
		interactionFadingOut = false;
		yield return displayWaitTime;
		interactionFadingIn = false;
		interactionFadingOut = true;
	}

	IEnumerator GunInstructions() {
		yield return firstDisplayWaitTime;
		interactionText.text = "Use left click to shoot, press R to reload";
		interactionFadingIn = true;
		interactionFadingOut = false;
		yield return displayWaitTime;
		StartCoroutine(GunThoughts());
		interactionFadingIn = false;
		interactionFadingOut = true;
	}

	IEnumerator GunThoughts() {
		yield return firstDisplayWaitTime;
		infoText.text = "A gun... I hope I don't have to use it.";
		infoFadingOut = false;
		infoFadingIn = true;
		yield return displayWaitTime;
		infoFadingIn = false;
		infoFadingOut = true;
	}

	public void UpdateAmmo(int remainingAmmo, int totalAmmo) {
		ammoText.text = remainingAmmo.ToString() + " | " + totalAmmo.ToString();
	}

	public void EndInteraction() {
		interactionFadingOut = true;
		interactionFadingIn = false;
	}

	void Update() {

		if(!gameStarted) {
			StartCoroutine(StartGame());
		}

		if(interactionFadingIn) {
			interactionText.color = Color.Lerp(interactionText.color, textColor, fadeSpeed * Time.deltaTime);
			if(interactionText.color.a >= 0.95f){
				interactionFadingIn = false;
			}
		}

		if(interactionFadingOut) {
			interactionText.color = Color.Lerp(interactionText.color, Color.clear, fadeSpeed * Time.deltaTime);
			if(interactionText.color.a == 0){
				interactionFadingOut = false;
			}
		}

		if(ammoFadingIn) {
			ammoText.color = Color.Lerp(ammoText.color, textColor, fadeSpeed * Time.deltaTime);
			if(ammoText.color.a >= 0.95f){
				ammoFadingIn = false;
			}
		}

		if(infoFadingIn) {
			infoText.color = Color.Lerp(infoText.color, textColor, fadeSpeed * Time.deltaTime);
			if(infoText.color.a >= 0.95f){
				infoFadingIn = false;
			}
		}

		if(infoFadingOut) {
			infoText.color = Color.Lerp(infoText.color, Color.clear, fadeSpeed * Time.deltaTime);
			if(infoText.color.a == 0){
				infoFadingOut = false;
			}
		}
	}



}
