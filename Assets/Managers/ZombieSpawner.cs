﻿using UnityEngine;
using System.Collections;

public class ZombieSpawner : MonoBehaviour {

	public int numberOfZombies = 0;

	[SerializeField] private GameObject[] zombies;
	[SerializeField] private float spawnTime = 10f;
	[SerializeField] private Transform[] spawnPoints;
	[SerializeField] private int maxZombies = 30;

	// Use this for initialization
	void Start () {
		InvokeRepeating("Spawn", spawnTime, spawnTime);
	}
	
	void Spawn() {
		int spawnPointIndex = Random.Range(0, spawnPoints.Length);
		int zombieToSpawn = Random.Range(0, zombies.Length);

		if(numberOfZombies < maxZombies) {
			Instantiate(zombies[zombieToSpawn], spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
			numberOfZombies++;
		}

	}
}
