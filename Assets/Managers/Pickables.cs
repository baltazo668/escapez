﻿using UnityEngine;
using System.Collections;

public class Pickables : MonoBehaviour {

	private UIDisplayManager uiDisplay;
	private PlayerArmsController armsControl;
	private GunShooting gunShoot;
	private UseFlare flare;
	private bool canPickUp = false;

	void Awake() {
		uiDisplay = GameObject.FindObjectOfType<UIDisplayManager>();
		armsControl = GameObject.FindObjectOfType<PlayerArmsController>();
		gunShoot = GameObject.FindObjectOfType<GunShooting>();
		flare = GameObject.FindObjectOfType<UseFlare>();
	}

	void OnTriggerEnter(Collider other) {
		uiDisplay.Interaction(gameObject.tag);
		canPickUp = true;
	}

	void OnTriggerExit(Collider other) {
		canPickUp = false;
		uiDisplay.EndInteraction();
	}

	void Update() {
		if(Input.GetButtonDown("Use") && canPickUp) {
			RemoveObject();
			if(gameObject.tag == "Flashlight") {
				armsControl.MovingLeftArm();
			}
			if(gameObject.tag == "Revolver" && uiDisplay.hasGun == true) {
				gunShoot.AddingAmmo(6);
				Debug.Log("Gun already picked up, adding ammo");
			}
			if(gameObject.tag == "Revolver" && uiDisplay.hasGun == false) {
				PickingUpGun();
			}
			if(gameObject.tag == "Flare") {
				uiDisplay.FlareFound();
				flare.flareFound = true;
			}

		}
	}

	void RemoveObject() {
		Destroy(gameObject);
		uiDisplay.EndInteraction();
	}

	void PickingUpGun() {
		armsControl.MovingRightArm();
		uiDisplay.DisplayAmmo();

	}
}