﻿using UnityEngine;
using System.Collections;

public class CursorUnlock : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
	}

}
