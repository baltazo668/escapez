﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Hover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	private Animator anim;

	void Start () {
		anim = GetComponentInParent<Animator>();
	}
	
	public void OnPointerEnter (PointerEventData eventData) {
		anim.SetBool(gameObject.name, true);
	}

	public void OnPointerExit (PointerEventData eventData) {
		anim.SetBool(gameObject.name, false);
	}
}
