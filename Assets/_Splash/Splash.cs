﻿using UnityEngine;
using System.Collections;

public class Splash : MonoBehaviour {

	private LevelManager levelManager;

	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}

	void Update() {
		if(Input.GetMouseButtonDown(0)) {
			StartGame();
		}
	}
	
	void StartGame() {
		levelManager.LoadNextLevel();
	}
}
