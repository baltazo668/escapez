﻿using UnityEngine;
using System.Collections;

public class TempCursor : MonoBehaviour {

	void Update() {
		if(Input.GetKeyDown(KeyCode.M)) {
			Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
		}
	}
}
