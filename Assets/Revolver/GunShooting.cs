﻿using UnityEngine;
using System.Collections;

public class GunShooting : MonoBehaviour {

	public int damage = 1;
	public GameObject hitParticles;
	public ParticleSystem shootFlare;
	public Transform gunPoint;
	public Camera playerCamera;
	public GameObject gunLight;
	public bool canShoot = false;
	public AudioClip gunShot;
	public AudioClip click;
	public AudioClip reload;
	public bool isPaused = false;

	[SerializeField] private float fireRate = 1.5f;
	[SerializeField] private float range = 500;

	private LineRenderer lineRenderer;
	private WaitForSeconds shotLength =  new WaitForSeconds(0.07f);
	private AudioSource audioSource;
	private UIDisplayManager uiDisplay;
	private PlayerArmsController armsControl;
	private float nextFireTime;
	private int spareAmmo = 0;
	private int remainingAmmo = 6;



	// Use this for initialization
	void Awake () {
		lineRenderer = GetComponent<LineRenderer>();
		audioSource = GetComponent<AudioSource>();
		uiDisplay = GameObject.FindObjectOfType<UIDisplayManager>();
		armsControl = GameObject.FindObjectOfType<PlayerArmsController>();
	}
	
	// Update is called once per frame
	void Update () {

		if(!canShoot && armsControl.gunFound) {
			if(Input.GetButtonDown("Fire") && Time.time > nextFireTime) {
				nextFireTime = Time.time + fireRate;
				audioSource.clip = click;
				audioSource.Play();
			}
		}

		if(!canShoot && spareAmmo > 0){
			canShoot = true;
		}

		if(canShoot) {
			RaycastHit hit;
			Vector3 rayOrigin = playerCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));

			if(Input.GetButtonDown("Fire") && Time.time > nextFireTime) {
				if(isPaused) {
					return;
				}
				nextFireTime = Time.time + fireRate;
				lineRenderer.SetPosition(0, gunPoint.position);


				if(Physics.Raycast(rayOrigin, playerCamera.transform.forward, out hit, range)) {
					//If the raycast hits something, check if it's a zombie
					ZombieDamage dmgScript = hit.collider.gameObject.GetComponent<ZombieDamage>();

					if(dmgScript != null) {
						//If it is, do 1 damage
						dmgScript.Damage(damage, hit.point);
					}

					//Render a line from the gun to the hit point and instantiate the particles
					lineRenderer.SetPosition(1, hit.point);
					Instantiate(hitParticles, hit.point, Quaternion.identity);


				} else {
					lineRenderer.SetPosition(1, (gunPoint.forward * range + gunPoint.position)); //If the raycast doesn't hit anything, render the line from the end of the gun
				}

				//Plays the sound effect and gun flare, etc
				StartCoroutine(ShotEffect());
				remainingAmmo--;
				uiDisplay.UpdateAmmo(remainingAmmo, spareAmmo);
				if(remainingAmmo == 0) {
					Recharge();
				}
			}

			if(Input.GetButtonDown("Reload")) {
				Recharge();
			}
		}
	}

	private void Recharge() {
		canShoot = false;
		if(spareAmmo == 0) { // If there is some ammo left in the gun but none to spare
			if(remainingAmmo > 0){
				audioSource.clip = click;
				audioSource.Play();
				canShoot = true;
			}
		} else if(spareAmmo == 0 && remainingAmmo == 0) { // If there is no ammo left
			audioSource.clip = click;
			audioSource.Play();
		} else if(remainingAmmo == 6) { // If there is already 6 ammo in the gun
			canShoot = true;
		} else { // If there is less than six ammo in the gun and some ammo to spare
			int ammoToReload = 6 - remainingAmmo;
			if(ammoToReload > spareAmmo) {
				ammoToReload = spareAmmo;
			}
			remainingAmmo += ammoToReload;
			spareAmmo -= ammoToReload;
			uiDisplay.UpdateAmmo(remainingAmmo, spareAmmo);
			armsControl.Recharging();
			audioSource.clip = reload;
			audioSource.PlayDelayed(0.5f);
		}

	}

	private IEnumerator ShotEffect() {
		shootFlare.Play();
		gunLight.SetActive(true);
		lineRenderer.enabled = true;
		audioSource.clip = gunShot;
		audioSource.Play();
		yield return shotLength;
		gunLight.SetActive(false);
		lineRenderer.enabled = false;
	}

	public void AddingAmmo(int ammo){
		spareAmmo += ammo;
		uiDisplay.UpdateAmmo(remainingAmmo, spareAmmo);
	}
}
